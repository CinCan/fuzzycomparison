# Fuzzycomparison

### Detect changes on webpages using ssdeep fuzzy hash and Levenshtein edit distance

Fuzzycomparison takes a JSON file containing webpage URLs and previous results from those webpages, downloads those pages if they are still available, and reports changes based on the ssdeep hash and Levenshtein edit distance.

#### JSON format
```
[
    {
      "Levenshtein edit distance": integer,
      "Levenshtein relative edit distance": float, proportional edit distance,
      "comparison value": integer, ssdeep comparison value,
      "error code": previous error code received while fetching,
      "hash": ssdeep hash of the latest downloaded version,
      "hash-old": ssdeep hash of the previous downloaded version,
      "human-readable timestamp": ISO timestamp, the time the latest version was downloaded,
      "human-readable timestamp-old": ISO timestamp, the time the previous version was downloaded,
      "previous version": string containing the previous downloaded version of the page ,
      "timestamp": Epoch timestamp, the time the latest version was downloaded,
      "timestamp-old": Epoch timestamp, the time the previous version was downloaded,
      "url": URL of the page
    },
]

```
Only the URL field is required when adding new webpages to the JSON, the script will create and populate the rest as needed.

### Usage
#### Standalone:

***1. Clone the repository or download the fuzzycomparison.py***
```
git clone https://gitlab.com/CinCan/tools
cd tools/fuzzycomparison
```
```
wget https://gitlab.com/CinCan/tools/raw/master/fuzzycomparison/fuzzycomparison.py
```
***2. Run the tool***
```
python3 fuzzycomparison.py <path to JSON file> <output path>
```
#### Dockerfile:

***1. Pull the docker image***
```
docker pull cincan/fuzzycomparison
```

If you wish, you can also build the image yourself. 
```
wget https://gitlab.com/CinCan/tools/raw/master/fuzzycomparison/dockerfile
docker build . -t cincan/fuzzycomparison
```

***2. Run the docker container***

```
docker run cincan/fuzzycomparison <path to JSON file> <output path>
```

### Command line parameters

`-h, --help: show this help message and exit`

##### inputs:
`-c, --cached: use only cached files (not downloading new)`

`-i, --input: path to input json file`

`-u, --url: URL or URLs to scan`

`-H, --hash: ssdeep hash for URL`

`-e, --edit-distance: previous version of a webpage for edit distance`

##### outputs:

`-o filepath, --output filepath: output path`

##### options:

`v, --verbose: increase output Verbosity`

`-V, --version: show program's version number and exit`

##### configuration:
These adjust the default configuration

`--config: path to alternate configuration file`

`--ssdeep-tolerance: Tolerance for ssdeep, float between 0.0-1.0`

`--distance-tolerance: threshold for edit distance, float between 0.0-1.0`

`--archive-interval ARCHIVE_INTERVAL: Time interval for archiving copies of the webpage, formatted as 00d00h00m00s`

`--cache-count: number of cached versions to be stored`



### Configuration:

`'UseStdin': use stdin for input (default false)`

`'UseStdout': use stdout for output (default false)`

`'UseEditDistance': calculate edit distance (default true)`

`'UseSsdeep': calculate ssdeep hash (defeault true)`,

`'UseOnlyCache': use only cached files (do not download a new version) (default false)`

`'ChangesOutputPath': filepath for reporting changes in websites (default changes.json)`

`'DefaultOutputPath': default output filepath, used if no other path is supplied (default fuzzycomparison.json),`

`'CachePath': filepath for edit distance cache (default webpage_cache)`

`'MaxCacheCount': maximum amount of cached versions (default 1)`

`'MaxSnapshotCount': maximum amount of ssdeep snapshots stored in the output JSON (default 10)`

`'ssdeep_tolerance': maximum tolerance for deviating from the standard deviation for ssdeep (0.0 - 1.0, default 0.1)`

`'edit_distance_tolerance': maximum tolerance for deviating from the standard deviation for edit distance (0.0 - 1.0, default 0.1)`

`'DefaultVerbosity': default verbosity for output (1 - 3, default 1)`

`'archive_interval': time interval for caching webpages in seconds (default 30000)`

