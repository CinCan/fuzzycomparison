FROM ubuntu:18.04

LABEL MAINTAINER=cincan.io

COPY requirements.txt requirements.txt

RUN buildDeps='build-essential libffi-dev python3-dev python3-setuptools python3-pip wget' \
    && apt update && apt install -y --no-install-recommends $buildDeps \
    git \
    libfuzzy-dev \
    python3 \
    && pip3 install -r requirements.txt \
    && useradd --no-log-init --create-home appuser \
    && cd /home/appuser \
    && mkdir app \
    && cd app \
    && git clone git@gitlab.com:CinCan/fuzzycomparison.git \
    && rm -rf /var/lib/apt/lists/* \
    && apt purge -y --auto-remove $buildDeps

WORKDIR /home/appuser

USER appuser

ENTRYPOINT ["python3", "/app/fuzzycomparison.py"]

