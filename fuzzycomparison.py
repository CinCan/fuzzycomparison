import sys
import argparse

from modules import config
from modules import util
from modules import cache
from modules import deviation
from modules import history
from modules import input_output
from modules import parsing


def argument_parsing():
    """Parse the command line arguments"""

    parser = argparse.ArgumentParser(description='scan URLs for changes in '
                                     'the content using ssdeep hashing and edit '
                                     'distance')
    inputs = parser.add_argument_group('inputs')
    outputs = parser.add_argument_group('outputs')
    options = parser.add_argument_group('options')
    # commands = parser.add_argument_group('commands')
    configuration_arguments = parser.add_argument_group('configuration')

    #commands.add_argument('commands', choices=['list', 'config'])

    inputs.add_argument('-c', '--cached', dest='cached_only', action='store',
                        help='use only cached files (not downloading new)')
    inputs.add_argument('-i', '--input', dest='input_path', action='store',
                        help='path to input json file', metavar='filepath')
    inputs.add_argument('-u', '--url', dest='input_url', action='store',
                        help='URL to scan', nargs='*', metavar='URL')
    inputs.add_argument('-H', '--hash', dest='input_hash', action='store',
                        help='ssdeep hash for URL', nargs='*', metavar='hash')
    inputs.add_argument('-e', '--edit-distance', dest='input_edit_distance', action='store',
                        help='previous version of a webpage for edit distance',
                        nargs='*', metavar='filepath')

    outputs.add_argument('-o', '--output', dest='output_path', action='store',
                         help='output path', metavar='filepath')

    options.add_argument('-v', '--verbose', dest='verbosity', action='count',
                         help='increase output Verbosity')
    options.add_argument('-V', '--version', action='version', version='%(prog)s 0.8')

    configuration_arguments.add_argument('--config', dest='config', action='store',
                                         help='path to configuration file')
    configuration_arguments.add_argument('--ssdeep-tolerance', dest='ssdeep_tolerance', action='store', type=float,
                                         help='Tolerance for ssdeep, float between 0.0-1.0', metavar='float')
    configuration_arguments.add_argument('--distance-tolerance', dest='edit_distance_tolerance', action='store',
                                         type=float, help='threshold for edit distance, float between 0.0-1.0',
                                         metavar='float')
    configuration_arguments.add_argument('--archive-interval', dest='archive_interval', action='store',
                                         help='Time interval for archiving copies of the webpage, formatted as 00d00h00m00s')
    configuration_arguments.add_argument('--cache-count', dest='cache_count', action='store', type=int,
                                         help='number of cached versions to be stored')

    return parser.parse_args()


def main():
    # Initialize  commandline arguments and configuration

    args = argument_parsing()
    configuration = config.initialize_config(args)
    verbosity = configuration['run-time']['Verbosity']

    # Initialize input
    if configuration['UseOnlyCache']:
        webpages, input_error = input_output.initialize_input_from_cache(configuration)
    else:
        webpages, input_error = input_output.initialize_input(args, configuration)
    if input_error is not None:
        util.verbose_print(1, verbosity, input_error)
        sys.exit(1)
    changes = []
    # Enumerate pages
    for i, webpage in enumerate(webpages):
        url = webpage['url']
        util.verbose_print(3, verbosity, 'Processing {}...'.format(url))

        removed = False
        ssdeep_changes = None
        edit_distance_changes = None
        cache_outdated = True
        ssdeep_history = None
        edit_distance_history = None
        timestamp = 0

        # Get the contents of the page either from the cache or by downloading the page
        # depending of the configuration
        if configuration['UseOnlyCache']:
            contents = cache.get_page_from_cache(url, configuration['run-time']['CachePath'])

        else:
            contents, code = util.download_page(url, configuration['run-time']['Header'])

            if contents is not None:
                contents = contents.read()
            else:
                removed = True
        if not removed:
            # Get the previous ssdeep hash if available, otherwise initialize the ssdeep history
            if 'ssdeep history' in webpage and len(webpage['ssdeep history']) > 0:
                hash_old = webpage['ssdeep history'][-1][1]
            else:
                hash_old = None
                webpage['ssdeep history'] = []
                ssdeep_changes = False

            # Initialize the edit distance history if needed
            if 'edit distance history' not in webpage or not configuration['UseEditDistance']:
                webpage['edit distance history'] = []
                edit_distance_changes = False

            # Parse the contents of the webpage
            snapshot_ssdeep, snapshot_edit_distance = parsing.parse_webpage(
                                                                    url,
                                                                    contents,
                                                                    hash_old,
                                                                    configuration['UseSsdeep'],
                                                                    configuration['UseEditDistance'],
                                                                    configuration['UseOnlyCache'],
                                                                    configuration['run-time']['CachePath'])
            if snapshot_ssdeep is not None and configuration['UseSsdeep']:
                util.verbose_print(3, verbosity, 'Ssdeep comparison value: {}'.format(snapshot_ssdeep[2]))
                # Store the latest snapshot to the history
                webpage['ssdeep history'] = history.handle_history(webpage['ssdeep history'],
                                                           snapshot_ssdeep,
                                                           configuration['MaxSnapshotCount'])
                ssdeep_history = [x[2] for x in webpage['ssdeep history'] if x[2] is not None]

            if snapshot_edit_distance is not None and configuration['UseEditDistance']:
                util.verbose_print(3, verbosity, 'Edit distance: {} - relative distance: {:.2f}'
                                   .format(snapshot_edit_distance[1], snapshot_edit_distance[2]))

                webpage['edit distance history'] = history.handle_history(webpage['edit distance history'],
                                                                  snapshot_edit_distance,
                                                                  configuration['MaxSnapshotCount'])
                timestamp = snapshot_edit_distance[0]
                edit_distance_history = [x[2] for x in webpage['edit distance history'] if x is not None]

            # If there is any history for ssdeep or edit distance, check for changes
            # and in case of edit distance, check if cache is outdated

            if ssdeep_history is not None \
                    and len(ssdeep_history) > 1\
                    and configuration['UseSsdeep']:

                ssdeep_changes, ssdeep_deviation = deviation.check_deviation(ssdeep_history, configuration['ssdeep_tolerance'])

            if edit_distance_history is not None \
                    and len(edit_distance_history) > 1\
                    and configuration['UseEditDistance']:
                edit_distance_changes, edit_distance_deviation = deviation.check_deviation(edit_distance_history,
                                                                  configuration['edit_distance_tolerance'])
                previous_timestamp = webpage['edit distance history'][-2][0]
                cache_outdated = previous_timestamp < (timestamp - configuration['archive_interval'])

        webpage_changed = edit_distance_changes or ssdeep_changes or removed

        cache_page = configuration['UseEditDistance'] and (webpage_changed or cache_outdated) and not removed

        if cache_page:
            cache.store_cache(webpage['url'],
                              timestamp,
                              contents,
                              configuration['run-time']['CachePath'],
                              configuration['MaxCacheCount'],
                              configuration['archive_interval'])

        if webpage_changed:
            changes.append(webpage)
            util.verbose_print(2, verbosity, 'Changes in webpage:')
            if removed:
                util.verbose_print(2, verbosity, '\tPage has been removed or could not be reached')
            if ssdeep_changes:
                util.verbose_print(2, verbosity, '\tssdeep comparison value outside of standard deviation')
            if edit_distance_changes:
                util.verbose_print(2, verbosity, '\tedit distance comparison value outside of standard deviation')

    output_error = input_output.output_results(webpages, filepath=configuration['run-time']['OutputPath'], use_stdout=configuration['UseStdout'])
    if output_error is not None:
        util.verbose_print(1, verbosity, output_error)

    if configuration['ChangesOutputPath'] is not None and changes:
        output_error = input_output.output_results(changes, filepath=['ChangesOutputPath'])
        if output_error is not None:
            util.verbose_print(1, verbosity, output_error)


if __name__ == "__main__":
    main()
