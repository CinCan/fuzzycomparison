from setuptools import setup

setup(
    name='fuzzycomparison',
    version='0.1',
    py_modules=['fuzzycomparison'],
    install_requires=[
    'edlib>=1.3.4',
    'ssdeep>=3.3',
    ],
    entry_points='''
        [console_scripts]
        fuzzycomparison=fuzzycomparison:main
    ''',
)