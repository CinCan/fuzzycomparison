def handle_history(history, snapshot, max_snapshot_count=None):
    """
    Store the snapshot of the analysis in the history and check the length of the history
    """
    if max_snapshot_count is not None and len(history) > max_snapshot_count:
        excess = len(history) - max_snapshot_count
        history = history[excess + 1:]

    history.append(snapshot)
    return history
