import sys
import os
import json
from json import JSONDecodeError
from .cache import get_new_urls


def add_url(url, webpages, hash=None):
    old_entry = False
    for webpage in webpages:
        if url in webpage['url']:
            old_entry = True
    if not old_entry:
        webpage = {'url': url}
        if hash is not None:
            webpage['ssdeep history'] = [(0, hash, None)]

    """if (args.input_hash is not None and
            len(args.input_hash) >= i and
            configuration['UseSsdeep']):
        webpage['hash'] = args.input_hash[i]"""

    return webpage


def initialize_input(args, configuration):
    """Initialize all inputs of URLs and hashes or previous versions of the page"""
    webpages = []
    error = None

    if configuration['UseStdin'] and configuration['run-time']['InputPath'] is None:
        try:
            webpages = json.load(sys.stdin)

        except JSONDecodeError:
            error = 'ERROR: malformed JSON in stdin'

    if configuration['run-time']['InputPath'] is not None:
        if os.path.splitext(configuration['run-time']['InputPath'])[-1] == '.json':
            try:
                with open(configuration['run-time']['InputPath'], 'r') as f:
                    webpages = json.load(f)

            except JSONDecodeError:
                error = 'ERROR: malformed JSON in input file'

            except FileNotFoundError:
                pass
        else:
            with open(configuration['run-time']['InputPath']) as f:
                for url in f.readlines():
                    webpages.append(add_url(url.strip(), webpages))

    if args.input_url is not None:
        for i, url in enumerate(args.input_url):
            webpages.append(add_url(url, webpages, args.input_hash[i]))

            """if (args.input_edit_distance is not None and
                    len(args.input_edit_distance) >= i and
                    config.configuration['UseEditDistance']):

                with open(args.input_edit_distance[i], 'r') as f:
                    previous_version = f.read()
                    webpage['previous version'] = previous_version"""
    if not webpages:
        error = 'ERROR: no input. See --help for more information'

    return webpages, error


def initialize_input_from_cache(configuration):
    webpages = []
    error = None

    if configuration['run-time']['InputPath'] is not None:
        try:
            with open(configuration['run-time']['InputPath'], 'r') as f:
                webpages = json.load(f)

        except JSONDecodeError:
            error = 'ERROR: malformed JSON in input file'

        except FileNotFoundError:
            pass
    urls = set(x['url'] for x in webpages if 'url' in x)

    new_urls = get_new_urls(configuration['CachePath'], urls)
    webpages += new_urls

    if not webpages:
        error = 'ERROR: no input. See --help for more information'

    return webpages, error


def output_results(webpages, filepath=None, use_stdout=False):
    """
    Output results to file or stdout
    """
    if filepath is not None:
        if os.path.isfile(filepath):
            old_entry = False
            try:
                with open(filepath, 'r') as f:
                    output = json.load(f)
                    for result_new in webpages:
                        for result_old in output:
                            if result_new['url'] == result_old['url']:
                                result_old.update(result_new)
                                old_entry = True
                                break
                        if not old_entry:
                            output.append(result_new)

                with open(filepath, 'w') as f:
                    json.dump(output, f, indent=2, sort_keys=True)

            except JSONDecodeError:
                return 'ERROR: malformed JSON in output file'

        else:
            with open(filepath, 'w') as f:
                json.dump(webpages, f, indent=2, sort_keys=True)
            return None

    else:
        if use_stdout:
            print(json.dumps(webpages, indent=2, sort_keys=True))
            return None
