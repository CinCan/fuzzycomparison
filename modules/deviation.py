from math import sqrt


def calculate_standard_deviation(values: list):
    """
    Calculate mean and standard deviation of given list of numbers
    :param values: list of numbers
    :return:
    """
    mean = sum(values) / len(values)
    deviation = sqrt(sum((x - mean) ** 2 for x in values) / len(values))
    return mean, deviation


def check_deviation(values: list, tolerance: float = 0):
    """
    Calculates if the last value in a list deviates from the mean of the list
    more than standard deviation and a given tolerance

    :param values: list of numbers
    :param tolerance: acceptable deviation from the standard deviation
    :return: boolean, true if last value is outside of acceptable deviation
    """
    mean, standard_deviation = calculate_standard_deviation(values)
    standard_deviation += tolerance
    min_value = mean - standard_deviation
    max_value = mean + standard_deviation

    return not min_value < values[-1] < max_value, standard_deviation
