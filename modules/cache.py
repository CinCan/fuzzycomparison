import os
import os.path
import hashlib


def get_new_urls(cache_path, urls):

    new_urls = []
    added_urls = []
    for dir in os.listdir(cache_path):
        dir_path = os.path.join(cache_path, dir)
        if os.path.isdir(dir_path):
            with open(dir_path[:-2], 'r') as f:
                url = f.readlines()[0]

            if url not in urls and url not in added_urls:
                new_urls.append({'url': url})
                added_urls.append(url)
    return new_urls


def filter_cached_pages(cache_path):
    return [f for f in os.listdir(cache_path)
            if os.path.isfile(os.path.join(cache_path, f))]



def store_cache(url, timestamp, input_file, cache_path, max_cache_count=None, archive_interval=None):
    url_filename = url.replace('/', '')
    url_file_path = os.path.join(cache_path, 'url')
    with open(url_file_path, 'w') as f:
        f.write(url)

    cache_path = os.path.join(cache_path, url_filename) + '.d'
    cache_filename = '{}_{}.txt'.format(timestamp, url_filename)
    cache_file_path = os.path.join(cache_path, cache_filename)

    if not os.path.exists(cache_path):
        os.makedirs(cache_path)

    cached_files = filter_cached_pages(cache_path)
    if max_cache_count is not None:
        if len(cached_files) > max_cache_count:
            excess = len(cached_files) - max_cache_count
            while excess > 0:
                excess -= 1
                os.remove(os.path.join(cache_path, cached_files[0]))

    if archive_interval is not None:
        if not os.path.isfile(cache_file_path):
            with open(cache_file_path, 'w') as f:
                f.write(str(input_file))


def get_page_from_cache(url, cache_path, version=-1):
    """
    Get cached version of the web page, by default the latest
    """
    latest_cache = None
    cache_contents = None
    filtered_url = url.replace('/', '')
    cache_path = os.path.join(cache_path, filtered_url) + '.d'
    try:
        cached_files = filter_cached_pages(cache_path)
        if len(cached_files) > 0:
            cache_path = os.path.join(cache_path, cached_files[version])
            with open(cache_path, 'r') as f:
                cache_contents = f.read()
        return cache_contents

    except FileNotFoundError:
        return None
