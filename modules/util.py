import re
import urllib.parse
import urllib.request
import urllib.error
from datetime import timedelta
from random import choice


def parse_timedelta(delta_string):
    """

    :param delta:
    :return:
    """
    regex = (r'((?P<days>\d+?)d)?'
             '((?P<hours>\d+?)hr?)?'
             '((?P<minutes>\d+?)m)?'
             '((?P<seconds>\d+?)s)?')
    regex = re.compile(regex)

    parts = regex.match(str(delta_string))

    parts = parts.groupdict()
    filtered_parts = {key: int(value) for key, value in parts.items() if value is not None}

    if filtered_parts == {}:
        return None
    delta = timedelta(**filtered_parts)

    if delta.total_seconds() >= 0:
        return delta.total_seconds()


def verbose_print(level, max_level, *output, **kwargs):
    """Wrapper for print() with verbosity level check"""
    if level <= max_level:
        print(*output, **kwargs)


def random_header():
    desktop_agents = ['Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                     'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                     'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                     'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/602.2.14 (KHTML, like Gecko) Version/10.0.1 Safari/602.2.14',
                     'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
                     'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
                     'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.98 Safari/537.36',
                     'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36',
                     'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36',
                     'Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0']

    return {'User-Agent': choice(desktop_agents),
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}


def download_page(url, headers):
    """Download given url, returning the resulting file and HTTP code or error"""

    if not urllib.parse.urlparse(url).scheme:
        url = 'http://' + url

    try:
        req = urllib.request.Request(url=url, headers=headers)
        site = urllib.request.urlopen(req)
        return site, site.getcode()

    except urllib.error.HTTPError as e:
        return None, e.code

    except urllib.error.URLError as e:
        if hasattr(e, 'reason'):
            return None, e.reason

        if hasattr(e, 'code'):
            return None, e.code
        return None, None

    except ValueError as e:
        print(e, url)
        return None, e