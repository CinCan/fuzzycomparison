import os
import sys
import json
from json import JSONDecodeError
from . import util

DEFAULT_CONFIG = os.environ.get('DEFAULT_CONFIG') or 'default.cfg'
DEFAULT_HEADER = os.environ.get('DEFAULT_HEADER') or util.random_header()

configuration = {
    'UseStdin': False,
    'UseStdout': False,
    'UseEditDistance': True,
    'UseSsdeep': True,
    'UseOnlyCache': False,
    'ChangesOutputPath': 'changes.json',
    'DefaultOutputPath': 'fuzzycomparison.json',
    'DefaultCachePath': 'webpage_cache',
    'MaxCacheCount': 1,
    'MaxSnapshotCount': 10,
    'ssdeep_tolerance': 0.1,
    'edit_distance_tolerance': 0.1,
    'DefaultVerbosity': 1,
    'archive_interval': 30000,
    'run-time': {
           'OutputPath': None,
           'InputPath': None,
           'Verbosity': 0,
           'Header': DEFAULT_HEADER,
           'CachePath': None
          }
     }


def initialize_config(args):
    """Initialize the config, load saved config and options given through parameters"""

    if os.path.isfile(DEFAULT_CONFIG):
        try:
            with open(DEFAULT_CONFIG, 'r') as f:
                configuration.update(json.load(f))

        except JSONDecodeError:
            util.verbose_print(1, configuration['run-time']['Verbosity'], 'ERROR: malformed JSON in default config file')
            sys.exit(1)

    if args.config is not None:
        try:
            with open(args.config) as f:
                configuration.update(json.load(f))
        except JSONDecodeError:
            util.verbose_print(1, configuration['run-time']['Verbosity'], 'ERROR: malformed JSON in config file')
            sys.exit(1)
    else:
        save_config(DEFAULT_CONFIG)

    if args.cached_only is not None:
        configuration['run-time']['CachePath'] = args.cached_only
    else:
        configuration['run-time']['CachePath'] = configuration['DefaultCachePath']

    if args.input_path is not None:
        configuration['run-time']['InputPath'] = args.input_path
    elif configuration['DefaultOutputPath'] is not None:
        configuration['run-time']['InputPath'] = configuration['DefaultOutputPath']

    if configuration['DefaultOutputPath'] is not None:
        configuration['run-time']['OutputPath'] = configuration['DefaultOutputPath']
    if args.output_path is not None:
        configuration['run-time']['OutputPath'] = args.output_path
    if args.archive_interval is not None:
        configuration['archive_interval'] = util.parse_timedelta(args.archive_interval)
        save_config(DEFAULT_CONFIG)
    if args.cache_count is not None:
        configuration['MaxCacheCount'] = args.cache_count
        save_config(DEFAULT_CONFIG)

    if not configuration['UseStdin'] and args.output_path is None:
        configuration['run-time']['OutputPath'] = configuration['run-time']['InputPath']

    if configuration['UseStdout']:
        configuration['run-time']['Verbosity'] = 0
    elif args.verbosity is not None:
        configuration['run-time']['Verbosity'] = int(args.verbosity)
    else:
        configuration['run-time']['Verbosity'] = configuration['DefaultVerbosity']
    return configuration


def save_config(file_path):
    updated_config = {key: configuration[key] for key in configuration if key != 'run-time'}
    with open(file_path, 'w') as f:
        json.dump(updated_config, f, indent=2, sort_keys=True)