from datetime import datetime
import ssdeep
import edlib
from . import cache


def parse_webpage(url, contents, hash_old, use_ssdeep, use_edit_distance, use_only_cache, cache_path):
    """
    Download given web page, calculate hashes and edit distances if possible
    """

    snapshot_ssdeep = None
    snapshot_edit_distance = None
    timestamp = round(datetime.now().timestamp())

    if use_ssdeep:
        hash_new, comparison_value = calculate_ssdeep(contents, hash_old)
        snapshot_ssdeep = (timestamp, hash_new, comparison_value)

    if use_edit_distance:
        edit_distance = 0
        relative_distance = 0
        if use_only_cache:
            cached_version = cache.get_page_from_cache(url, cache_path, -2)
        else:
            cached_version = cache.get_page_from_cache(url, cache_path)
        if cached_version is not None:
            edit_distance, relative_distance = calculate_edit_distance(contents, cached_version)

        snapshot_edit_distance = (timestamp, edit_distance, relative_distance)

    return snapshot_ssdeep, snapshot_edit_distance


def calculate_ssdeep(input_string, hash_old):
    try:
        hash_new = ssdeep.hash(input_string)
    except TypeError as e:
        return None, None
    comparison_value = None
    if hash_old is not None:
        comparison_value = ssdeep.compare(hash_old, hash_new)

    return hash_new, comparison_value


def calculate_edit_distance(input_string, comparison_string):
    try:
        edit_distance = edlib.align(input_string, comparison_string)
    except TypeError as e:
        return None, None
    relative_distance = 0
    if len(comparison_string) > 0:
        relative_distance = 1 - edit_distance['editDistance'] / len(comparison_string)

    return edit_distance['editDistance'], relative_distance
